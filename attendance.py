import xmlrpclib

def action_attendance(auth_data):
    common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(auth_data['url']))
    uid = common.authenticate(auth_data['db'], auth_data['user'], auth_data['password'], {})
    models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(auth_data['url']))
    result = models.execute_kw(auth_data['db'], uid, auth_data['password'], auth_data['model'], 'attendance_manual', [[uid], ['hr_attendance.hr_attendance_action_my_attendances']])
    print('Checked In: {}'.format(result['action']['attendance']['check_in']))
    print('Checked Out: {}'.format(result['action']['attendance']['check_out']))

if __name__ == '__main__':
    user_auth = {
            'url': <odoo_server_base_url>,
            'db': <database>,
            'user': <user_name>,
            'password': <password>,
            'model': 'hr.employee'
            }

    action_attendance(user_auth)
